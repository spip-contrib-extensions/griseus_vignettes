<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'griseusvignettes_nom' => 'Griseus : vignettes',
	'griseusvignettes_slogan' => 'Vignettes des types de fichier',
	'griseusvignettes_description' => 'Ce plugin fait partie du thème "Griseus". Il remplace les vignettes des types de fichiers (affichées au moyen de la balise #LOGO_DOCUMENT) par des vignettes dérivées du jeu d\'icônes "Faïence" de Matthieu James.'
);

?>